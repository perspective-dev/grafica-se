
var $newsletterOpen = false;

$(document).ready(function() {
  
    $('.hamburger').click(function(){
      $(this).toggleClass('is-active');
      $(".main-menu-fixed").toggleClass('visible');
      $("body").toggleClass('not-scroll');
    });

    $('#featured-games .slider').slick({
      mobileFirst: true,
      infinite: false,
      slidesToShow: 1,
      prevArrow: '<i class="fas fa-chevron-left arrow prev"></i>',
      nextArrow: '<i class="fas fa-chevron-right arrow next"></i>',
      initialSlide: 1,
      responsive: [
        {
          breakpoint: 767,
          settings: {
            
          }
        },
        {
           breakpoint: 992,
           settings: "unslick"
        }
     ]
    });

    $('.games .slider').slick({
      arrows: true,
      infinite: false,
      slidesToShow: 3,
      prevArrow: '<i class="fas fa-chevron-left arrow prev"></i>',
      nextArrow: '<i class="fas fa-chevron-right arrow next"></i>',
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2
          }
        },
        { 
          breakpoint: 576,
          settings: {
            slidesToShow: 1
          }
        }
      ]
    });

    $('.games .slider').on('setPosition', function () {
      var slickTrackHeight = $(this).find('.slick-track').height();
      $(this).find('.slick-slide').css('height', slickTrackHeight + 'px');
    });


    $('.facts .slider').slick({
      arrows: true,
      infinite: true,
      slidesToShow: 1,
      prevArrow: '<i class="fas fa-chevron-left arrow prev"></i>',
      nextArrow: '<i class="fas fa-chevron-right arrow next"></i>',
      responsive: [
        {
          breakpoint: 992,
          settings: {
            arrows: false,
            dots: true
          }
        }
      ]
    });

    newsletter();

    $('.newsletter .close').click(function(){
      $(".newsletter").fadeTo(300, 0, function(){
        $(".newsletter").slideUp();
        $(".newsletter-icon").removeClass("invisible");
        $newsletterOpen = false;
      });
    
    });

    $('.newsletter-icon').click(function(){
      $(".newsletter-icon").addClass("invisible");
      $(".newsletter").slideDown();
      $(".newsletter").fadeTo(300, 1);
    });

    $("span.new").addClass('jackInTheBox');

    $(".card").hover(function(){
      $("span.new").css("opacity", 1);
      $(this).find("span.new").removeClass('jackInTheBox');
    });

    //PANTONI
    
    $('.pantoni-wrapper .fa-times-circle').click(function(){
      $(this).parent().parent().fadeTo(300, 0, function(){
        $(this).remove();
      });
    });
    $('#pantone-green').click(function(){
      $("body").removeClass("red");
      $("body").removeClass("blue");
      $("body").addClass("green");
    });

    $('#pantone-red').click(function(){
      $("body").removeClass("green");
      $("body").removeClass("blue");
      $("body").addClass("red");
    });

    $('#pantone-blue').click(function(){
      $("body").removeClass("green");
      $("body").removeClass("red");
      $("body").addClass("blue");
    });

});

$(document).scroll(function() {
  newsletter();
});

function newsletter(){
  var offset = $(".newsletter").offset().top - $(window).height();

  if ($(document).scrollTop() >= offset) {
    if(!$newsletterOpen) {
      $(".newsletter").addClass("sticky").fadeTo( "slow", 1 );
      $newsletterOpen = true;
    }
    
  } else {
    if(!$newsletterOpen) {
      setTimeout(function(){
        $(".newsletter").addClass("sticky").fadeTo( "slow", 1 );
        $newsletterOpen = true;
      }, 2000);
    }
  }
}